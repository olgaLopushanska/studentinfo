package com.luxoft.rcp.handlers;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.luxoft.rcp.model.Student;
import com.luxoft.rcp.model.StudentModel;

public class SaveToFileHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) {
		List<Student> list = StudentModel.getModel().getStudentList();
		String fileName = "";
		Shell shell = null;
		try {
			shell = HandlerUtil.getActiveShellChecked(event);
			FileDialog dlg = new FileDialog(shell, SWT.SAVE);
			dlg.setFilterExtensions(new String[] { "*.json" });
			if (dlg.open() == null) {
				return null;
			}
			fileName = dlg.getFilterPath() + "\\" + dlg.getFileName();
		} catch (ExecutionException e) {
			System.err.println("Can not create dialog window");
			return null;
		}
		if (list == null || list.size() == 0) {
			showError("Model has no data", shell);
			return null;
		}
		saveToFile(fileName, list);
		System.out.println("Save to file item was called");
		return null;
	}

	private void showError(String str, Shell shell) {
		MessageDialog dialog = new MessageDialog(shell, "Student Info", null, str, MessageDialog.ERROR, new String[] {},
				0);
		dialog.open();
	}

	public void saveToFile(String fileName, List<Student> list) {
		ObjectMapper mapper = new ObjectMapper();
		try (FileWriter wr = new FileWriter(fileName)) {
			mapper.writeValue(wr, list);
		} catch (IOException e) {
			System.out.println("Can not write data to file");
		}
	}
}
