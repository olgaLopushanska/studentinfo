package com.luxoft.rcp.model;

import java.util.ArrayList;
import java.util.List;

public class StudentModel {
	private List<Student> studentList;
	private static StudentModel model = new StudentModel();

	private StudentModel() {
		studentList = new ArrayList<>();
		Student student = new Student("Vasya Bobrov", -2, new Address("Opp", 3, "Kyiv"), 4, "");
		Student student2 = new Student("Oleg Bobrov", 2, new Address("Oppp", 3, "Kharkiv"), 4, "");
		Student st = new Student("Kolya Orel", 2);
		studentList.add(student);
		studentList.add(student2);
		studentList.add(st);
	}

	public List<Student> getStudentList() {
		return List.copyOf(studentList);
	}

	public void addEntryToStudentList(Student student) {
		studentList.add(student);
	}

	public void setStudentList(List<Student> list) {
		studentList = list;
	}

	public static StudentModel getModel() {
		return model;
	}
}
