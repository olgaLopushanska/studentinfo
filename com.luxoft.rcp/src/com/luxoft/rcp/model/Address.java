package com.luxoft.rcp.model;

import java.util.Objects;

public class Address {
	// @JsonProperty("street")
	private String street;
	 //@JsonProperty("streetNumber")
	private int streetNumber;
	private String city;

	public Address() {
	}

	public Address(String street, int streetNumber, String city) {
		super();
		this.street = street;
		this.streetNumber = streetNumber;
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(int streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Address [street=" + street + ", streetNumber=" + streetNumber + ", city=" + city + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(city, street, streetNumber);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		return Objects.equals(city, other.city) && Objects.equals(street, other.street)
				&& streetNumber == other.streetNumber;
	}
}
