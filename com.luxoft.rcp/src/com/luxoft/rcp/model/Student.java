package com.luxoft.rcp.model;

import java.util.Objects;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Student {
	@JsonIgnore
	private long id;
	private String name;
	private int group;
	private Address address;
	private int result;
	private String photo;
	
	public Student() {
		setId();
	}

	public Student(String name, int group) {
		this(name, group, null, 0, "");
	}

	public Student(String name, int group, Address address, int result, String photo) {
		this();
		this.name = name;
		setGroup(group);
		this.address = address;
		setResult(result);
		this.photo = photo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(address, group, id, name, photo, result);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		return Objects.equals(address, other.address) && group == other.group && id == other.id
				&& Objects.equals(name, other.name) && Objects.equals(photo, other.photo) && result == other.result;
	}

	public String getName() {
		return name;
	}

	public int getGroup() {
		return group;
	}

	public Address getAddress() {
		return address;
	}

	public int getResult() {
		return result;
	}

	public String getPhoto() {
		return photo;
	}

	private void setId() {
		this.id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
	}

	public long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGroup(int group) {
		this.group = group > 0 ? group : 0;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setResult(int result) {
		this.result = (result >= 0 && result < 13) ? result : 1;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", group=" + group + ", address=" + address + ", result="
				+ result + ", photo=" + photo + "]";
	}
}
