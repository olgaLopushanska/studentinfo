package com.luxoft.rcp.tests;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.luxoft.rcp.handlers.ReadFromFileHandler;
import com.luxoft.rcp.model.Address;
import com.luxoft.rcp.model.Student;

public class ReadFromFileHandlerTest {

	private ObjectMapper mapper;
	private ReadFromFileHandler handler;
	private final String fileName = "src\\file.json";;
	private FileWriter myWriter;

	@BeforeEach
	public void init() {
		mapper = new ObjectMapper();
		handler = new ReadFromFileHandler();
		File file = new File(fileName);
		try {
			file.createNewFile();
			myWriter = new FileWriter(fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void readFileWithCorrectStudentTest() throws IOException {
		List<Student> expected = List.of(new Student("Vasya Bobrov",3,new Address("Irva", 3,"Kyiv"),4,""));
		mapper.writeValue(myWriter, expected);
		List<Student> actual = (List<Student>) handler.parseJson(fileName);
		Assertions.assertEquals(expected.get(0).getName(), actual.get(0).getName());
		Assertions.assertEquals(expected.get(0).getGroup(), actual.get(0).getGroup());
		Assertions.assertEquals(expected.get(0).getAddress(), actual.get(0).getAddress());
		Assertions.assertEquals(expected.get(0).getResult(), actual.get(0).getResult());
	}

	@Test
	public void readFileWithMinimalRequiredStudentFieldsTest() throws IOException {
		List<Student> expected = List.of(new Student("Vasya Bobrov",3));
		mapper.writeValue(myWriter, expected);
		List<Student> actual = (List<Student>) handler.parseJson(fileName);
		Assertions.assertEquals(expected.get(0).getName(), actual.get(0).getName());
		Assertions.assertEquals(expected.get(0).getGroup(), actual.get(0).getGroup());
	}

	@Test
	public void readFileWithDefaultStudentFieldsTest() throws IOException {
		List<Student> expected = List.of(new Student("", 0));
		mapper.writeValue(myWriter, expected);
		List<Student> actual = (List<Student>) handler.parseJson(fileName);
		Assertions.assertEquals(List.of(), actual);
	}

	@Test
	public void readEmptyFileTest() throws IOException {
		mapper.writeValue(myWriter, "");
		List<Student> actual = (List<Student>) handler.parseJson(fileName);
		Assertions.assertEquals(null, actual);
	}

	@Test
	public void readFileWithIncorrectStudentFieldsTest() throws IOException {
		Student student = new Student("Vasya Bobrov",3,new Address("Irva", 3,"Kyiv"),-4,"");
		Student studentExpected = new Student("Vasya Bobrov",3,new Address("Irva", 3,"Kyiv"),0,"");
		List<Student> initial = List.of(student);
		List<Student> expected = List.of(studentExpected);
		mapper.writeValue(myWriter, initial);
		List<Student> actual = (List<Student>) handler.parseJson(fileName);
		Assertions.assertEquals(expected.get(0).getName(), actual.get(0).getName());
		Assertions.assertEquals(expected.get(0).getGroup(), actual.get(0).getGroup());
		Assertions.assertEquals(expected.get(0).getAddress(), actual.get(0).getAddress());
		Assertions.assertEquals(expected.get(0).getResult(), actual.get(0).getResult());

	}

	@Test
	public void readFileWithInsufficientStudentFields() throws IOException {
		Student student = new Student("Vasya Bobrov", 0);
		List<Student> initial = List.of(student);
		mapper.writeValue(myWriter, initial);
		List<Student> actual = (List<Student>) handler.parseJson(fileName);
		Assertions.assertEquals(List.of(), actual);
	}

	@Test
	public void readFileWithStudentWithAdditionalIncorrectFieldsTest() throws IOException {
		String json = "[{\"name\":\"Vasya Bobrov\",\"group\":3,\"class\":5, \"studentId\":3}]";
		mapper.writeValue(myWriter, json);
		List<Student> actual = (List<Student>) handler.parseJson(fileName);
		Assertions.assertEquals(null, actual);
	}

	@AfterEach
	public void clean() {
		try {
			myWriter.close();
		} catch (IOException e) {
			System.out.println("Can not close fileWriter from RedFromFileHandlerTest");
		}
		File file = new File(fileName);
		file.delete();
	}
}
