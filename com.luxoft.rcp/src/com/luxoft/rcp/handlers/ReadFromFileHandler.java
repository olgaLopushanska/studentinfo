package com.luxoft.rcp.handlers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.luxoft.rcp.model.Student;
import com.luxoft.rcp.model.StudentModel;

public class ReadFromFileHandler extends AbstractHandler {
	private String errorMessage = null;

	@Override
	public Object execute(ExecutionEvent event) {
		String fileName;
		Shell shell;
		try {
			shell = HandlerUtil.getActiveShellChecked(event);
			FileDialog dlg = new FileDialog(shell, SWT.SINGLE);
			dlg.setFilterExtensions(new String[] { "*.json" });
			if (dlg.open() == null) {
				return null;
			}
			fileName = dlg.getFilterPath() + "\\" + dlg.getFileName();
		} catch (ExecutionException e) {
			System.err.println("Can not create dialog window");
			return null;
		}
		List<Student> list = parseJson(fileName);
		if (list != null) {
			StudentModel.getModel().setStudentList(list);
		}
		if (errorMessage != null) {
			showError(errorMessage, shell);
			errorMessage = null;
		}
		System.out.println("Read from file item was called");
		return null;
	}

	private void showError(String str, Shell shell) {
		MessageDialog dialog = new MessageDialog(shell, "Student Info", null, str, MessageDialog.ERROR, new String[] {},
				0);
		dialog.open();
	}

	public List<Student> parseJson(String fileName) {
		List<Student> list = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		Student[] studentArray = null;
		try (FileReader fr = new FileReader(fileName)) {
			studentArray = mapper.readValue(fr, Student[].class);
		} catch (FileNotFoundException e) {
			errorMessage = "Could not read file";
			System.out.println(errorMessage);
			return null;
		} catch (Exception e) {
			errorMessage = "Could not convert json to student data";
			System.out.println(errorMessage);
			return null;
		}

		list = Arrays.stream(studentArray).filter(student -> student.getName() != null)
				.filter(student -> student.getName().trim().length() > 0 && student.getGroup() != 0).toList();
		System.out.println(list);
		return list;
	}
}