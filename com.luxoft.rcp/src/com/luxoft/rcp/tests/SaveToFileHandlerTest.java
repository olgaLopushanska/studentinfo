package com.luxoft.rcp.tests;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.luxoft.rcp.handlers.SaveToFileHandler;
import com.luxoft.rcp.model.Address;
import com.luxoft.rcp.model.Student;

public class SaveToFileHandlerTest {
	private SaveToFileHandler handler = new SaveToFileHandler();
	private final String fileName = "src\\file.json";

	@Test
	public void readFileWithCorrectStudentTest() throws IOException {
		Student student = new Student("Vasya Bobrov", 3, new Address("Irva", 3, "Kyiv"), 4, "");
		List<Student> list = List.of(student);
		handler.saveToFile(fileName, list);
		String expected = "[{\"name\":\"Vasya Bobrov\",\"group\":3,\"address\":{\"street\":\"Irva\",\"streetNumber\":3,"
				+ "\"city\":\"Kyiv\"},\"result\":4,\"photo\":\"\"}]";
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
		String actual = reader.readLine();
		reader.close();
		assertEquals(expected, actual);
	}
}
