package com.luxoft.rcp.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;

public class HelpHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) {
		System.out.println("Help menu item was called");
		return null;
	}
}

